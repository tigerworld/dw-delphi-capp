unit umain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ucsmsg;

type
  TForm1 = class(TForm)
    lst: TListBox;
    btn1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    csmsg : TCSMsg;

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  csmsg := TCSMsg.create;
end;

procedure TForm1.btn1Click(Sender: TObject);
var    
  msgfield : PCSMsgField;
  buf : array[0..1024] of char;
  msglen : integer;
begin
  //pack
  ZeroMemory(@buf, 1024);
  csmsg.command := $01;
  csmsg.pack(buf, msglen);

  //unpack  
  if not csmsg.unpack(buf, msglen) then
  begin
    lst.Items.Add('解包失败：' + csmsg.errmsg);
    Exit;
  end;
  lst.Items.Add('解包成功，请求命令字:' + inttostr(30+csmsg.command));    
  lst.Items.Add('---------end-------------');

end;

end.
