{ 带数据库操作的基类
}
unit udbbase;

interface
uses
  SysUtils, Math, Types, DateUtils, ADODB, utypes, ubase, UpAdoQuery;

type
  TDbBase = class(tbase)
  public    
    fpadocon : PUpAdoConnection;
    //数据库连接
    qry : TUpADOQuery;
    qry1 : TUpADOQuery;
    qry2 : TUpADOQuery;
    qry3 : TUpADOQuery;
    qry4 : TUpADOQuery;
    qry5 : TUpADOQuery;
  public
    constructor Create(padocon : PUpAdoConnection);
    destructor Destroy(); override;
  end;


implementation

{ TDbBase }

constructor TDbBase.Create(padocon : PUpAdoConnection);
begin
  self.fpadocon := padocon;

  //create qry
  qry := TUpADOQuery.Create(nil);
  qry.Connection := padocon^;

  //create qry1
  qry1 := TUpADOQuery.Create(nil);
  qry1.Connection := padocon^;

  //create qry2
  qry2 := TUpADOQuery.Create(nil);
  qry2.Connection := padocon^;

  //create qry3
  qry3 := TUpADOQuery.Create(nil);
  qry3.Connection := padocon^;

  //create qry4
  qry4 := TUpADOQuery.Create(nil);
  qry4.Connection := padocon^;

  //create qry5
  qry5 := TUpADOQuery.Create(nil);
  qry5.Connection := padocon^;
end;


destructor TDbBase.Destroy;
begin
  qry.Close;
  FreeAndNil(qry);

  qry1.Close;
  FreeAndNil(qry1);

  qry2.Close;
  FreeAndNil(qry2);

  qry3.Close;
  FreeAndNil(qry3);

  qry4.Close;
  FreeAndNil(qry4);

  qry5.Close;
  FreeAndNil(qry5);
end;

end.
