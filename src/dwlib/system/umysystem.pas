//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  umysystem.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:                                            
//      1、系统控制类，有一个全局的实例在uglobal单元声明；
//      2、管理系统配置类实例、根据配置文件初始化数据库连接对象
//      TAdoConnection;
//      3、用户登录，授权目录下载；
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit umysystem;

interface
uses
  Windows, SysUtils, Classes, Math, Types, DateUtils, udbbase, utypes, ADODB,
  inifiles, Forms, udbconfigfile, umyconfig, utblyuang, ukeyvaluelist, ucsmsg,
  UpUdp, WinSock, Tlhelp32;

var     
  { 2013-3-21：在frmbase.onshow中设置控件的输入法 }
  strshurufa_default :string;
  //默认输入法;本地参数中配置，读取本地参数的时候获取；
  
type
  TAppendChildForm = procedure(frm : Tform) of object;
  TShowSystemMessage = procedure(strmessage : string) of object;
  
  { 系统定义
  }               
  PMySystem = ^TMySystem;
  TMySystem = class(TDbBase)
  private
  public
    runlevel : string;
    //运行级别; 在主配置文件[myhisclient]节点下runlevel字段配置
    //release;debug;...; 系统初始化init中完成读取;

    mainconfig : TMyConfig;
    //系统主配置文件

    localserver_ip : string;
    localserver_port : Integer;

    cliendid : string;            //医院编码
    cliendname : string;          //医院名称
    shengjxh : string;           //系统升级序号
    shengjxh_server : string;    //服务器系统升级序号
    dbupdate_server_string : string; //升级数据库db串
    dbconnectionstring : string;  //数据库连接串
    
    loginyuang : TLoginYuang;
    //当前登录员工

    local_sysparams : TKeyValueList;
    //本地系统参数
    global_sysparams : TKeyValueList;
    //全局系统参数
  private
    procedure readmainconfig();
    //读取主配置文件信息

    procedure connectdb();
    //连接数据库

  public
  //2012-7-1：在系统参数设置中保存参数值后需要刷新内存参数
  //  故再次提升了下面两个函数的访问限制;
    procedure readglobalparams();
    //读取全局参数
    procedure readlocalparams();
    //读取本机参数

    function init() : boolean;
    //应用初始化

  public
    FAppendChildForm : TAppendChildForm;
    FShowSystemMessage : TShowSystemMessage;
  public
    constructor Create(padocon : PUpAdoConnection);
    destructor Destroy(); override;
  end;

  
function KillTask(ExeFileName: string): Integer;

implementation

uses ubase;

function KillTask(ExeFileName: string): Integer;
const
  PROCESS_TERMINATE = $0001;
var
  ContinueLoop: boolean;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  Result := 0;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) = UpperCase(ExeFileName))
      or (UpperCase(FProcessEntry32.szExeFile) = UpperCase(ExeFileName))) then
      Result := Integer(TerminateProcess(OpenProcess(PROCESS_TERMINATE,BOOL(0),FProcessEntry32.th32ProcessID),0));
    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;

{ TSystem }

procedure TMySystem.connectdb;
var
  str, strconnectionstring: string;
  dbconfigfile : TDBConfigFile;
  buffer : array[0..1024] of char;
  len : integer;
begin
//hisserver
{$IFDEF HISSERVER}
  //连接业务数据库
  dbconfigfile := TDBConfigFile.create(mainconfig.ReadString('system', 'dbconnfile', ''));
  zeromemory(@buffer, 1024);
  if not dbconfigfile.readconfigstring(buffer, len) then
  begin
    str := '读取业务数据库配置串失败：' + inttostr(dbconfigfile.errcode) + '---' + dbconfigfile.errmsg;
    freeandnil(dbconfigfile);
    raise Exception.Create(str);
  end;
  freeandnil(dbconfigfile);

  setlength(strconnectionstring, len);
  copymemory(pchar(strconnectionstring), @buffer, len);

  if fpadocon^.Connected then
    fpadocon^.Close;

  fpadocon^.ConnectionString := strconnectionstring;
{$ENDIF}

//intf_IYBLZDR   
{$IFDEF IYBLZDR}
  //连接业务数据库
  dbconfigfile := TDBConfigFile.create(mainconfig.ReadString('system', 'dbconnfile', ''));
  zeromemory(@buffer, 1024);
  if not dbconfigfile.readconfigstring(buffer, len) then
  begin
    str := '读取业务数据库配置串失败：' + inttostr(dbconfigfile.errcode) + '---' + dbconfigfile.errmsg;
    freeandnil(dbconfigfile);
    raise Exception.Create(str);
  end;
  freeandnil(dbconfigfile);

  setlength(strconnectionstring, len);
  copymemory(pchar(strconnectionstring), @buffer, len);

  if fpadocon^.Connected then
    fpadocon^.Close;

  fpadocon^.ConnectionString := strconnectionstring;
{$ENDIF}

//intf_xinnh_gs_file
{$IFDEF INTF_XINNH_GS_FILE}
  //连接业务数据库
  dbconfigfile := TDBConfigFile.create(mainconfig.ReadString('system', 'dbconnfile', ''));
  zeromemory(@buffer, 1024);
  if not dbconfigfile.readconfigstring(buffer, len) then
  begin
    str := '读取业务数据库配置串失败：' + inttostr(dbconfigfile.errcode) + '---' + dbconfigfile.errmsg;
    freeandnil(dbconfigfile);
    raise Exception.Create(str);
  end;
  freeandnil(dbconfigfile);

  setlength(strconnectionstring, len);
  copymemory(pchar(strconnectionstring), @buffer, len);

  if fpadocon^.Connected then
    fpadocon^.Close;

  fpadocon^.ConnectionString := strconnectionstring;
{$ELSE}
  fpadocon^.ConnectionString := dbconnectionstring;
{$ENDIF}

  fpadocon^.Open;   
end;

function TMySystem.init(): boolean;
begin
  Result := False;
  try
    { 读取配置文件
    }
    readmainconfig();
                      
    { 初始化数据库连接
    }
    connectdb();

    { 读取系统参数
    }
    //本机参数
    readlocalparams();
    //全局参数
    readglobalparams();
    
    //$$$服务器不用检测升级更新
{$IFDEF HISSERVER}

{$ELSE}       
    //本地升级序号；
    shengjxh := mainconfig.ReadString('system','sys_update_id','0');
                    
    //读取服务器版本
    qry1.Connection := fpadocon^;
    qry1.OpenSql(Format('select isnull(max(shengjxh),0) from xt_shengjjl', []));
    shengjxh_server := qry1.getString(0);
    qry1.Close;
             
    //读取服务器最新系统版本号，不一致则强制升级
    if not SameText(shengjxh,shengjxh_server) then
    begin
      //服务器指定要升级到指定的版本

      self.dbupdate_server_string := fpadocon^.ConnectionString;
      errcode := -999;
      raise Exception.Create('检测到系统组件需要升级，请等待升级完成后重新登录系统！');
    end;     
{$ENDIF} 
          
    Result := True;
  except
    on e:Exception do
    begin
      errmsg :='服务初始化失败：' + e.Message;
      Exit;
    end;
  end;
end;

procedure TMySystem.readmainconfig;
var
  str, strconfigfile : string;
begin
  str := ExtractFilePath(application.ExeName) +
      ExtractFileName(Application.ExeName);
  strconfigfile := StringReplace(str, '.exe', '.ini', [rfIgnoreCase]);
  
  mainconfig := TMyConfig.Create(strconfigfile);

  //读取系统运行级别配置;
  runlevel := mainconfig.ReadString('myhisclient', 'runlevel', 'release');
end;

procedure TMySystem.readglobalparams;
begin
  { 读取参数有问题就通过异常通知上面
    必要的参数未配置等等错误.
  }
  global_sysparams.clearitem;

  qry.Close;
  qry.SQL.Text := 'select * from sys_xitcs';
  qry.Open;
  qry.First;
  while not qry.Eof do
  begin
    global_sysparams.additem(qry.fieldbyname('cansmc').AsString,
        qry.fieldbyname('cansz').AsString);
    qry.Next;
  end;
  qry.Close;
end;

procedure TMySystem.readlocalparams;
begin
  local_sysparams.clearitem;

  //report_bshowpreview=1
  local_sysparams.additem('report_bshowpreview',
      mainconfig.ReadString('localparams', 'report_bshowpreview', '1'));
                                   
  //acrobatfilename
  local_sysparams.additem('acrobatfilename',
      mainconfig.ReadString('localparams', 'acrobatfilename', '0'));

  //navbar字体读取  
  local_sysparams.additem('navbar_main_groupheader_fontsize',
      mainconfig.ReadString('frmmain', 'navbar_main_groupheader_fontsize', '11'));
  local_sysparams.additem('navbar_main_item_fontsize',
      mainconfig.ReadString('frmmain', 'navbar_main_item_fontsize', '11'));

  local_sysparams.additem('navbar_sub_groupheader_fontsize',
      mainconfig.ReadString('frmmain', 'navbar_sub_groupheader_fontsize', '11'));
  local_sysparams.additem('navbar_sub_item_fontsize',
      mainconfig.ReadString('frmmain', 'navbar_sub_item_fontsize', '11'));
  
end;

constructor TMySystem.Create(padocon: PUpAdoConnection);
begin
  inherited;
  
  //本机参数
  local_sysparams := TKeyValueList.Create;

  //全局参数
  global_sysparams := TKeyValueList.Create;

  //当前登录员工对象
  loginyuang := TLoginYuang.Create(padocon);
end;

destructor TMySystem.Destroy;
begin
  if Assigned(local_sysparams) then
    FreeAndNil(local_sysparams);

  if Assigned(global_sysparams) then
    FreeAndNil(global_sysparams);

  //销毁当前登录员工对象
  if Assigned(loginyuang) then
    FreeAndNil(loginyuang);

  if Assigned(mainconfig) then
    FreeAndNil(mainconfig);

  inherited;
end;

end.
