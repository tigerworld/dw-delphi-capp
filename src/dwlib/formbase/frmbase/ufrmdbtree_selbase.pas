//*******************************************************************
//  pmyhisgroup  ptblmanage   
//  ufrmdbtree_selbase.pas     
//                                          
//  创建: changym by 2012-02-22 
//        changup@qq.com                    
//  功能说明:
//      目录树选择窗体;
//      调用前必须先设置表名dbtree_tablename;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************
unit ufrmdbtree_selbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbdialogbase, DB, ADODB, Buttons, UpSpeedButton, ExtCtrls,
  UPanel, ImgList, ComCtrls, frxClass, frxDesgn, frxDBSet,
  UpDataSource, Menus, UpPopupMenu, UpAdoTable, UpAdoQuery, frxExportXLS,
  frxExportPDF, udbtree;

type
  Tfrmdbtree_selbase = class(Tfrmdbdialogbase)
    tvshangpfl: TTreeView;
    qrytree: TUpADOQuery;
    imglstlisttree: TImageList;
    procedure tvshangpflChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure tvshangpflGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure tvshangpflDblClick(Sender: TObject);
  private
    dbtree : TDBTree;

  protected   
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;
  public
    dbtree_tablename : string; 
    //表名       
    wherestr: string;
    orderbystr: string;

  public
    ret_ifenlbm : Integer;
    //返回的节点编码
    ret_strfenleifullbm, ret_strfenleifullname : string;
    //返回的节点编码全名\名称全名
  end;

//==============================================================================
// 2012-4-5, 注释以下声明, frmdbtree_selbase在需要使用的地方局部定义;
//==============================================================================
{
var
  frmdbtree_selbase: Tfrmdbtree_selbase;
}

implementation

{$R *.dfm}

{ Tfrmdbtree_selbase }

function Tfrmdbtree_selbase.Execute_dll_showmodal_after: Boolean;
begin
  Result := False;
    
  if ModalResult = mrok then
  begin
    if tvshangpfl.Selected <> nil then
    begin
      ret_ifenlbm := qrytree.fieldbyname('bianm').AsInteger;
      ret_strfenleifullbm := dbtree.getnodefullbm(ret_ifenlbm) + '/';
      ret_strfenleifullname := self.dbtree.getnodefullname(ret_ifenlbm);

      Result := True;
    end;
  end;
  
  qrytree.Close;
end;

function Tfrmdbtree_selbase.Execute_dll_showmodal_before: Boolean;
begin
  //创建目录树对象  
  dbtree := TDBTree.Create(qrytree, dbtree_tablename, tvshangpfl, dllparams.padoconn);
  //只罗列参加评审的部门列表
  dbtree.wherestr:= ' ' + wherestr + ' ';
  dbtree.showtree;

  tvshangpfl.AutoExpand := true;

  Width := 486;
  height := 406;
  Position := poScreenCenter;

  Result := True;
end;

procedure Tfrmdbtree_selbase.tvshangpflChanging(Sender: TObject;
  Node: TTreeNode; var AllowChange: Boolean);
begin
  inherited;
                  
  dbtree.treechange(node);
end;

procedure Tfrmdbtree_selbase.tvshangpflGetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;
  { 改变节点图标
  }
  if Node.Selected then
  begin
    Node.ImageIndex := 1;
  end
  else
  begin
    Node.ImageIndex := 2;
  end;
end;

procedure Tfrmdbtree_selbase.tvshangpflDblClick(Sender: TObject);
begin
  inherited;

  if tvshangpfl.Selected <> nil then
    btnyesClick(btnyes);
end;

end.
