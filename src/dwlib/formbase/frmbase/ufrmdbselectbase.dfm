inherited frmdbselectbase: Tfrmdbselectbase
  Left = 293
  Top = 98
  Caption = 'frmdbselectbase'
  ClientHeight = 505
  ClientWidth = 830
  PixelsPerInch = 96
  TextHeight = 13
  inherited bvl1: TBevel
    Top = 463
    Width = 830
  end
  inherited pfrmtop: TUPanel
    Top = 466
    Width = 830
    Height = 39
    inherited btnyes: TUpSpeedButton
      Caption = #36873#25321
    end
    inherited btnquit: TUpSpeedButton
      Left = 388
    end
  end
  object updvtlpnl1: TUpAdvToolPanel [2]
    Left = 0
    Top = 0
    Width = 830
    Height = 463
    Align = alClient
    BackgroundTransparent = False
    BackGroundPosition = bpTopLeft
    BorderWidth = 1
    Button3D = False
    HoverButtonColor = 16571329
    HoverButtonColorTo = 16565398
    DownButtonColor = 16571329
    DownButtonColorTo = 16565398
    CaptionButton = False
    Color = 16571329
    ColorTo = 16571329
    GradientDirection = gdVertical
    DockDots = True
    CanSize = False
    Caption = #21015#34920
    CaptionGradientDirection = gdVertical
    FocusCaptionFontColor = clBlack
    FocusCaptionColor = 16571329
    FocusCaptionColorTo = 16565398
    NoFocusCaptionFontColor = clBlack
    NoFocusCaptionColor = 16571329
    NoFocusCaptionColorTo = 16565398
    CloseHint = 'Close panel'
    LockHint = 'Lock panel'
    UnlockHint = 'Unlock panel'
    Sections = <>
    SectionLayout.CaptionColor = 16244422
    SectionLayout.CaptionColorTo = 14060643
    SectionLayout.CaptionFontColor = 8661248
    SectionLayout.CaptionRounded = False
    SectionLayout.Corners = scRectangle
    SectionLayout.BackGroundColor = 16248798
    SectionLayout.BackGroundColorTo = 16242365
    SectionLayout.BorderColor = clWhite
    SectionLayout.BorderWidth = 1
    SectionLayout.BackGroundGradientDir = gdVertical
    SectionLayout.Indent = 4
    SectionLayout.Spacing = 4
    SectionLayout.ItemFontColor = 11876608
    SectionLayout.ItemHoverTextColor = 11876608
    SectionLayout.ItemHoverUnderline = True
    SectionLayout.UnderLineCaption = False
    ShowCaptionBorder = False
    ShowClose = False
    ShowLock = False
    Style = esCustom
    Version = '1.5.0.1'
    OrgHeight = 0
    object upnltop: TUPanel
      Left = 2
      Top = 25
      Width = 826
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object lblsel: TUpLabel
        Left = 10
        Top = 6
        Width = 78
        Height = 13
        Caption = #35831#36755#20837#31616#25340#65306
      end
      object btnselect: TUpSpeedButton
        Left = 215
        Top = 2
        Width = 26
        Height = 20
        Flat = True
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000120B0000120B00000000000000000000FF00FF314B62
          AC7D7EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FF5084B20F6FE1325F8CB87E7AFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF32A0FE37A1FF
          106FE2325F8BB67D79FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FF37A4FE379FFF0E6DDE355F89BB7F79FF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          37A4FE359EFF0F6FDE35608BA67B7FFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF38A5FE329DFF156DCE444F5BFF
          00FF9C6B65AF887BAF887EAA8075FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FF3BABFFA1CAE7AD8679A98373E0CFB1FFFFDAFFFFDDFCF8CFCCB2
          9FA1746BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC0917DFC
          E9ACFFFFCCFFFFCFFFFFD0FFFFDEFFFFFAE3D3D1996965FF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFB08978FAD192FEF4C2FFFFD0FFFFDAFFFFF6FFFF
          FCFFFFFCB69384FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB08978FEDA97ED
          B478FBEEBBFFFFD3FFFFDCFFFFF4FFFFF4FFFFE2E9DDBCA67B73FF00FFFF00FF
          FF00FFFF00FFFF00FFB18A78FFDE99E9A167F4D199FEFCCCFFFFD5FFFFDAFFFF
          DCFFFFD7EFE6C5A97E75FF00FFFF00FFFF00FFFF00FFFF00FFAA7F73FAE0A4F0
          B778EEBA7BF6DDA6FEFBCCFFFFD3FFFFD1FFFFD7D9C5A7A3756CFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFCEB293FFFEDDF4D1A5EEBA7BF2C78FF8E1ABFCF0
          BAFCFACAA3776FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA1746BE1
          D4D3FFFEEEF7CC8CF0B473F7C788FCE3A5C2A088A5776CFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FF986865BA9587EAD7A4EAD59EE0C097A577
          6CA5776CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFA77E70A98073A4786EFF00FFFF00FFFF00FFFF00FF}
        alignleftorder = 0
        alignrightorder = 0
      end
      object edtseljianp: TUpEdit
        Left = 88
        Top = 2
        Width = 127
        Height = 21
        Ctl3D = True
        ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        ParentCtl3D = False
        TabOrder = 0
        OnKeyUp = edtseljianpKeyUp
        AutoInit = False
        HistoryValueOnOff = True
      end
    end
    object dbg: TUpWWDbGrid
      Left = 2
      Top = 52
      Width = 826
      Height = 409
      IniAttributes.Delimiter = ';;'
      TitleColor = 16571329
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      Color = clWhite
      DataSource = ds
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ImeMode = imDisable
      ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
      KeyOptions = []
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnDblClick = dbgDblClick
      OnKeyDown = dbgKeyDown
      FooterColor = clWhite
      FooterCellColor = clWhite
      PaintOptions.ActiveRecordColor = 14656898
    end
  end
  inherited qry: TUpAdoQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM v_yaof_pandjl')
    Left = 224
    Top = 136
  end
  inherited ds: TDataSource
    DataSet = qry
    Left = 256
    Top = 136
  end
end
