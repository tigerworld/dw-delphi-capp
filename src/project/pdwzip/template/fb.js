function annotMerger() {
	try {
		var conn = ADBC.newConnection("pdfreview", global.db_user, global.db_pass);
		var myStatement = conn.newStatement();
		var myStateExecute = conn.newStatement();
		var strsql = "";
		var strsql_exe = "";
		strsql = "select pingsbm from ls_wendhb_ext where jiqbs='" + global.hostid + "'";
		myStatement.execute(strsql);
		try {
			myStatement.nextRow();
			firstRow = myStatement.getRow()
		} catch(e) {
			firstRow = null
		}
		if (firstRow == null) {
			throw ("未找到正在准备上会文档的评审记录！");
		}
		var pingsbm = firstRow.pingsbm.value;
		strsql = "select pingszzxm from v_pings where bianm=" + pingsbm;
		myStatement.execute(strsql);
		try {
			myStatement.nextRow();
			row = myStatement.getRow()
		} catch(e) {
			row = null
		}
		if (!row) {
			throw ("未找到评审记录！");
		}
		doc_setfieldvalue(this, "edtpingszz", row.pingszzxm.value);
		strsql = "select * from ls_wendhb_ext_mx where pingsbm=" + pingsbm;
		myStatement.execute(strsql);
		try {
			myStatement.nextRow();
			row = myStatement.getRow()
		} catch(e) {
			row = null
		}
		var iline = 1;
		while (row) {
			var bianm_mingxwj = row.bianm.value
			var zhuanjwd_filename = row.zhuanjwjlj.value;
			var zhuanjwd_filename_anot = zhuanjwd_filename + '.fdf';
			var otherdoc = app.openDoc(zhuanjwd_filename, this, '', false);
			otherdoc.syncAnnotScan();
			myfun_exportAnnot(otherdoc, zhuanjwd_filename_anot);
			if (otherdoc) {
				otherdoc.closeDoc()
			}
			this.importAnFDF(zhuanjwd_filename_anot);
			try {
				myStatement.nextRow();
				row = myStatement.getRow()
			} catch(e) {
				row = null
			}
		}
		strsql = 'delete from ls_pings_wend_zhus where pingsbm=' + pingsbm;
		myStatement.execute(strsql);
		strsql = 'update ls_pings set zhussfzl=0 where bianm=' + pingsbm;
		myStatement.execute(strsql);
		this.syncAnnotScan();
		for (var i = 0; i < this.numPages; i++) {
			var annots = this.getAnnots(i);
			if (annots == null) {
				continue
			}
			annots.forEach(function(annot, index, arr) {
				var props = annot.getProps();
				var strsql = util.printf("insert into ls_pings_wend_zhus(pingsbm,page,name,uitype,intent,author,contents,creationDate,inReplyTo,modDate,state,stateModel)  values(%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", pingsbm, i + 1, props.name, annot.uiType, props.intent, props.author, props.contents, props.creationDate, props.inReplyTo, props.modDate, props.state == undefined ? "": props.state, props.stateModel == undefined ? "": props.stateModel);
				myStatement.execute(strsql)
			})
		}
		strsql_exe = 'update ls_wendhb_ext set jind=100 where pingsbm=' + pingsbm;
		myStatement.execute(strsql_exe)
	} catch(e) {
		app.alert('发生未知错误：' + e)
	}
}
function importannots() {
	try {
		var conn = ADBC.newConnection("pdfreview", global.db_user, global.db_pass);
		var myStatement = conn.newStatement();
		var myStateExecute = conn.newStatement();
		var strsql = "";
		var strsql_exe = "";
		strsql = "select pingsbm from ls_pings_pingsh_ext where jiqbs='" + global.hostid + "'";
		myStatement.execute(strsql);
		try {
			myStatement.nextRow();
			firstRow = myStatement.getRow()
		} catch(e) {
			firstRow = null
		}
		if (firstRow == null) {
			throw ("未找到集中评审会记录！");
		}
		var pingsbm = firstRow.pingsbm.value;
		strsql = 'delete from ls_pings_wend_zhus where pingsbm=' + pingsbm;
		myStatement.execute(strsql);
		strsql = 'update ls_pings set zhussfzl=0 where bianm=' + pingsbm;
		myStatement.execute(strsql);
		this.syncAnnotScan();
		for (var i = 0; i < this.numPages; i++) {
			var annots = this.getAnnots(i);
			if (annots == null) {
				continue
			}
			annots.forEach(function(annot, index, arr) {
				var props = annot.getProps();
				var strsql = util.printf("insert into ls_pings_wend_zhus(pingsbm,page,name,uitype,intent,author,contents,creationDate,inReplyTo,modDate,state,stateModel)  values(%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", pingsbm, i + 1, props.name, annot.uiType, props.intent, props.author, props.contents, props.creationDate, props.inReplyTo, props.modDate, props.state == undefined ? "": props.state, props.stateModel == undefined ? "": props.stateModel);
				myStatement.execute(strsql)
			})
		}
		strsql_exe = 'update ls_pings_pingsh_ext set jind=100 where pingsbm=' + pingsbm;
		myStatement.execute(strsql_exe)
	} catch(e) {
		app.alert('发生未知错误：' + e)
	}
}
function importinfo() {
	try {
		var conn = ADBC.newConnection("pdfreview", global.db_user, global.db_pass);
		var myStatement = conn.newStatement();
		var myStateExecute = conn.newStatement();
		var strsql = "";
		var strsql_exe = "";
		strsql = "select pingsbm from ls_pings_xinj_ext where jiqbs='" + global.hostid + "'";
		myStatement.execute(strsql);
		try {
			myStatement.nextRow();
			row = myStatement.getRow()
		} catch(e) {
			row = null
		}
		if (row == null) {
			throw ("未找到新创建的评审记录！");
		}
		var pingsbm = row.pingsbm.value;
		strsql = "select * from v_pings where bianm=" + pingsbm;
		myStatement.execute(strsql);
		try {
			myStatement.nextRow();
			row = myStatement.getRow()
		} catch(e) {
			row = null
		}
		if (!row) {
			throw ("未找到评审记录！");
		}
		doc_setfieldvalue(this, "edtpingsmc", row.mingc.value);
		doc_setfieldvalue(this, "edtxiangmdh", row.xiangmdh.value);
		doc_setfieldvalue(this, "edtxiangmmc", row.xiangmmc.value);
		doc_setfieldvalue(this, "edtyanzjd", row.yanzjd.value);
		doc_setfieldvalue(this, "edtpingslb", row.pingslxmc.value);
		doc_setfieldvalue(this, "edtpingsjb", row.pingsjbms.value);
		doc_setfieldvalue(this, "edtpingszz", row.pingszzxm.value);
		doc_setfieldvalue(this, "edtneiwbbs", row.neiwbms.value);
		doc_setfieldvalue(this, "edtbeipbm", row.beipbmmc.value);
		doc_setfieldvalue(this, "edtzuoz", row.zuozxm.value);
		strsql = "select * from v_pings_jiancx where pingsbm=" + pingsbm;
		myStatement.execute(strsql);
		try {
			myStatement.nextRow();
			row = myStatement.getRow()
		} catch(e) {
			row = null
		}
		if (!row) {
			throw ("未找到评审检查项信息！");
		}
		line = 1;
		var jiancxitem = '';
		while (row) {
			if (line > 30) break;
			jiancxitem = 'jiancx' + line;
			doc_setfieldvalue(this, jiancxitem, row.jiancxnr.value);
			line++;
			try {
				myStatement.nextRow();
				row = myStatement.getRow()
			} catch(e) {
				row = null
			}
		}
		strsql_exe = 'update ls_pings_xinj_ext set jind=100 where pingsbm=' + pingsbm;
		myStatement.execute(strsql_exe);
		conn.close()
	} catch(e) {
		if (conn) {
			conn.close()
		}
		console.println(e)
	}
}
function doc_setfieldvalue(doc, fieldname, fieldvalue) {
	var f = doc.getField(fieldname);
	if (f) {
		f.value = fieldvalue
	}
}