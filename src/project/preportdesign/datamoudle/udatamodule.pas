//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  udatamodule.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:                                            
//      数据模块；持有一个数据库连接对象TAdoConnection的实例;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit udatamodule;

interface

uses
  Windows, SysUtils, Classes, DB, ADODB, udbconfigfile;

type
  Tdm = class(TDataModule)
    conn: TADOConnection;
  private
  public
  end;

var
  dm: Tdm;

implementation

{$R *.dfm}

{ Tdm }

end.
