inherited frmxitgl_quanxgl: Tfrmxitgl_quanxgl
  Left = 83
  Top = 54
  Width = 1122
  Height = 616
  Caption = #31995#32479#26435#38480#31649#29702
  PixelsPerInch = 96
  TextHeight = 13
  inherited tbmain: TRzToolbar
    Width = 1114
    RowHeight = 31
  end
  object pg1: TRzPageControl [1]
    Left = 0
    Top = 39
    Width = 1114
    Height = 550
    ActivePage = tsTabSheet2
    Align = alClient
    TabHeight = 26
    TabIndex = 1
    TabOrder = 2
    TabStyle = tsCutCorner
    FixedDimension = 26
    object tsTabSheet1: TRzTabSheet
      Color = 16571329
      Caption = #35282#33394#23450#20041
      object pupl1: TUPanel
        Left = 0
        Top = 0
        Width = 249
        Height = 523
        Align = alLeft
        BevelOuter = bvLowered
        ParentColor = True
        TabOrder = 0
        object lbl2: TUpLabel
          Left = 14
          Top = 49
          Width = 65
          Height = 13
          Caption = #35282#33394#21517#31216#65306
        end
        object lbl3: TUpLabel
          Left = 14
          Top = 72
          Width = 65
          Height = 13
          Caption = #35282#33394#25551#36848#65306
        end
        object pupl4: TUPanel
          Left = 1
          Top = 1
          Width = 247
          Height = 35
          Align = alTop
          ParentColor = True
          TabOrder = 0
          object btn1: TUpSpeedButton
            Left = 4
            Top = 3
            Width = 77
            Height = 29
            Caption = #28155#21152
            Flat = True
            Glyph.Data = {
              9A020000424D9A020000000000009A0100002800000010000000100000000100
              08000000000000010000120B0000120B0000590000005900000000000000FFFF
              FF00FF00FF00AB00AB008B008B0079017900FF03FF00FA2FFA00FA95FA00F75F
              F4003C133900D7B48C00C2A07200D7A56000E7961100EB9F2200D08A0A00C495
              4100D1921200E7CC6200F0DD8F00F3E39E00FFFFEE007D8F0A00748904001566
              0000146801000087000000830000008200000081000000740000006E0000006A
              000000690000005E0000004C0000004B0000004400000473040000510200018A
              0300038A0600046606004370440000530300025E050004910B0001760800067A
              0E00077F1200468C4D000A9E1A000C9E1D0012811E0014A628002A9A3A000DA1
              230019962D0021A737000AAA27000F8E2700209635001BA138002CD8510016B5
              3A0018B23C0019B8420025BD50002CC6550025CB580027CB59002BD15E0036DA
              67003AE76F0039E9720045F680004DFF880040D7760055D4810078C79E00005F
              4F0005817F003ACCCB0000C4CE000D9AA40036EBFF0038EDFF0000D8FF000202
              02020202022525020202020202020202020202022E4C1E250202020202020202
              020202022E4C1E250202020202020202020202022E4C1E250202020202020202
              020202022E4C1E250202020202020202020202022E4437250202020202020202
              0202022E4843392F250202020202020202022E4A4742352A1D25020202020202
              022E4D4B464134291C1B2502020202022E49453F3D32272122201F250202022E
              2B191A30385354523E3B3A3125022E23180F0E173C4F5658554E40503624022E
              0D1513102D28335751262C090A0202020B161412020202020202080603050202
              020C11020202020202020807040202020202020202020202020202080202}
            OnClick = btn1Click
            alignleftorder = 0
            alignrightorder = 0
          end
          object btn2: TUpSpeedButton
            Left = 85
            Top = 3
            Width = 77
            Height = 29
            Caption = #20462#25913
            Flat = True
            Glyph.Data = {
              9A020000424D9A020000000000009A0100002800000010000000100000000100
              08000000000000010000120B0000120B0000590000005900000000000000FFFF
              FF00FF00FF00AB00AB008B008B0079017900FF03FF00FA2FFA00FA95FA00F75F
              F4003C133900D7B48C00C2A07200D7A56000E7961100EB9F2200D08A0A00C495
              4100D1921200E7CC6200F0DD8F00F3E39E00FFFFEE007D8F0A00748904001566
              0000146801000087000000830000008200000081000000740000006E0000006A
              000000690000005E0000004C0000004B0000004400000473040000510200018A
              0300038A0600046606004370440000530300025E050004910B0001760800067A
              0E00077F1200468C4D000A9E1A000C9E1D0012811E0014A628002A9A3A000DA1
              230019962D0021A737000AAA27000F8E2700209635001BA138002CD8510016B5
              3A0018B23C0019B8420025BD50002CC6550025CB580027CB59002BD15E0036DA
              67003AE76F0039E9720045F680004DFF880040D7760055D4810078C79E00005F
              4F0005817F003ACCCB0000C4CE000D9AA40036EBFF0038EDFF0000D8FF000202
              02020202022525020202020202020202020202022E4C1E250202020202020202
              020202022E4C1E250202020202020202020202022E4C1E250202020202020202
              020202022E4C1E250202020202020202020202022E4437250202020202020202
              0202022E4843392F250202020202020202022E4A4742352A1D25020202020202
              022E4D4B464134291C1B2502020202022E49453F3D32272122201F250202022E
              2B191A30385354523E3B3A3125022E23180F0E173C4F5658554E40503624022E
              0D1513102D28335751262C090A0202020B161412020202020202080603050202
              020C11020202020202020807040202020202020202020202020202080202}
            OnClick = btn2Click
            alignleftorder = 0
            alignrightorder = 0
          end
          object btn4: TUpSpeedButton
            Left = 166
            Top = 3
            Width = 77
            Height = 29
            Caption = #21024#38500
            Flat = True
            Glyph.Data = {
              9A020000424D9A020000000000009A0100002800000010000000100000000100
              08000000000000010000120B0000120B0000590000005900000000000000FFFF
              FF00FF00FF00AB00AB008B008B0079017900FF03FF00FA2FFA00FA95FA00F75F
              F4003C133900D7B48C00C2A07200D7A56000E7961100EB9F2200D08A0A00C495
              4100D1921200E7CC6200F0DD8F00F3E39E00FFFFEE007D8F0A00748904001566
              0000146801000087000000830000008200000081000000740000006E0000006A
              000000690000005E0000004C0000004B0000004400000473040000510200018A
              0300038A0600046606004370440000530300025E050004910B0001760800067A
              0E00077F1200468C4D000A9E1A000C9E1D0012811E0014A628002A9A3A000DA1
              230019962D0021A737000AAA27000F8E2700209635001BA138002CD8510016B5
              3A0018B23C0019B8420025BD50002CC6550025CB580027CB59002BD15E0036DA
              67003AE76F0039E9720045F680004DFF880040D7760055D4810078C79E00005F
              4F0005817F003ACCCB0000C4CE000D9AA40036EBFF0038EDFF0000D8FF000202
              02020202022525020202020202020202020202022E4C1E250202020202020202
              020202022E4C1E250202020202020202020202022E4C1E250202020202020202
              020202022E4C1E250202020202020202020202022E4437250202020202020202
              0202022E4843392F250202020202020202022E4A4742352A1D25020202020202
              022E4D4B464134291C1B2502020202022E49453F3D32272122201F250202022E
              2B191A30385354523E3B3A3125022E23180F0E173C4F5658554E40503624022E
              0D1513102D28335751262C090A0202020B161412020202020202080603050202
              020C11020202020202020807040202020202020202020202020202080202}
            OnClick = btn4Click
            alignleftorder = 0
            alignrightorder = 0
          end
        end
        object edtjiaosmc: TUpEdit
          Left = 79
          Top = 45
          Width = 160
          Height = 21
          ImeName = #20013#25991'('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
          MaxLength = 20
          TabOrder = 1
          AutoInit = False
          HistoryValueOnOff = True
        end
        object mmojiaosms: TUpMemo
          Left = 79
          Top = 72
          Width = 160
          Height = 77
          Color = clWhite
          ImeName = #20013#25991'('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
          MaxLength = 60
          TabOrder = 2
          HistoryValueOnOff = True
        end
      end
      object dbg1: TUpWWDbGrid
        Left = 249
        Top = 0
        Width = 272
        Height = 523
        Selected.Strings = (
          'bianm'#9'6'#9#32534#30721
          'mingc'#9'12'#9#21517#31216
          'miaos'#9'18'#9#25551#36848#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 16571329
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        Color = 16571329
        DataSource = dsjiaosdy
        ImeName = #20013#25991'('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        ReadOnly = True
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = GB2312_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = #23435#20307
        TitleFont.Style = []
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
      end
      object dbgjiaosyg: TUpWWDbGrid
        Left = 521
        Top = 0
        Width = 589
        Height = 523
        Selected.Strings = (
          'xingm'#9'12'#9#22995#21517#9#9
          'xingbms'#9'6'#9#24615#21035#9'F'
          'nianl'#9'6'#9#24180#40836#9'F'
          'bummc'#9'16'#9#37096#38376#9'F'
          'jiaosmc'#9'16'#9#35282#33394#9'F'
          'denglzh'#9'12'#9#30331#24405#24080#21495#9'F'
          'zhuangtms'#9'8'#9#29366#24577#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 16571329
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        Color = 16571329
        DataSource = dsjiaosyg
        ImeName = #20013#25991'('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        PopupMenu = upmnSelyuang_jiaosdy
        ReadOnly = True
        TabOrder = 2
        TitleAlignment = taCenter
        TitleFont.Charset = GB2312_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = #23435#20307
        TitleFont.Style = []
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
      end
    end
    object tsTabSheet2: TRzTabSheet
      Color = 16571329
      Caption = #35282#33394#26435#38480#35774#32622
      object pupl6: TUPanel
        Left = 0
        Top = 0
        Width = 1110
        Height = 31
        Align = alTop
        ParentColor = True
        TabOrder = 0
        object btnjiaosqx: TUpSpeedButton
          Left = 7
          Top = 3
          Width = 81
          Height = 25
          Caption = #20445#23384'(&S)'
          Flat = True
          Glyph.Data = {
            8E030000424D8E030000000000008E0200002800000010000000100000000100
            08000000000000010000120B0000120B0000960000009600000000000000FFFF
            FF00FF00FF00F6F4F600EEEAE700DED9D500F8F4EF00F8FAF600FBFEFF00C5ED
            FF00FCFEFF00A4DCFC00A7DEFF00E9F6FE0041AFFC0045B4FE005EB8F8007FC5
            FA0081C7FA00CCE7FB00D9E6F000F8FCFF003AA0F60076BFFA00CBE6FC00CAE5
            FB00E6F2FC00E3EEF7002091F3002293F4002490F2003298F2003599F300369C
            F40077BCF8007FBDF60089C4F7008EC7F800C9E3FB00E9F3FC00117EEE001884
            F0001C84EE001D89EF001E87EF00288AEF004F9FF00082BCF60087C0F600A7CF
            F700809EBC00CBE3FB00EAF3FC00137AED00137AEA00147BEB001680EE001A82
            EE001F80EB004586CF00529EF400579EEE005CA5F2005EA5F2005EA3F0005EA4
            F00062A6F20066A7EF0070AFF40073B0F40076B2F4005B89BC005580B00070A9
            E6005E8CBF007AB5F60077B1F0006392C70080B7F3009ECAF8009DC7F600B5D0
            EE00B7D3F000C5DEFA00C4DDF800CCE2FA00CFDAE600E7F2FE00EBF4FE00086C
            E7000C73E9000D73EA001176EA001674E7001878E900207DE9003284EA003587
            EA003688ED00378BEB004F99ED004787D1003D73B400589EEF005A9FEF005CA0
            EF0064A6F2000363E500096AE7000D67E2000F6AE5000F6BE500106AE5000F6A
            E0000F6ADE001169DE00136DE5001872E600297EE9003584EA00F8FBFF00005F
            E500025CE300025CE200035CE200035BDE000460E300045BDE00034BB500034B
            B4000763E300043E900008479F00094FAF000C5DCF001169E5000055E1000059
            E1000055E000003A99000142A5000241A400004FE0000050E0000051E0000052
            E0000053E0000054E000004EDE00FEFEFF000202020202838383838383020202
            02020202028080863B4D4A66848B830202020202807F4C031452515605488D83
            020202817D4F0167877B7A876906328C830202804B95768E8F6175908E620747
            8302727378689092640A4E9489926A048583723F275D7E410101558288936E1B
            6583722F265C2E013363346091887C544983722418392D233A7942508A887C53
            46837222581F2928355A6C31749170573C83722008111C1E2A37595F3D8E4315
            6D8302720B95101D2B385B6B6F770A45800202720E090112212C365E40014F7D
            80020202720F0C010D13191A01447F8002020202027272161725303E71808002
            020202020202027272727272720202020202}
          OnClick = btnjiaosqxClick
          alignleftorder = 0
          alignrightorder = 0
        end
      end
      object pupl2: TUPanel
        Left = 0
        Top = 31
        Width = 202
        Height = 489
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object upl_jiaosqx_set_title: TUPanel
          Left = 0
          Top = 0
          Width = 202
          Height = 24
          Align = alTop
          Alignment = taLeftJustify
          Caption = ' '#35282#33394#12304#12305
          ParentColor = True
          TabOrder = 0
        end
        object lstjiaosdy: TUpListBox
          Left = 0
          Top = 24
          Width = 202
          Height = 465
          Align = alClient
          ImeName = #20013#25991'('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
          ItemHeight = 13
          TabOrder = 1
          OnDblClick = lstjiaosdyDblClick
          AutoInit = False
          HistoryValue = -1
          HistoryValueOnOff = True
        end
      end
      object upc2: TUpPageControl
        Left = 202
        Top = 31
        Width = 908
        Height = 489
        ActivePage = tsxitgl
        Align = alClient
        Style = tsFlatButtons
        TabOrder = 2
        object tsxitgl: TTabSheet
          Caption = #31995#32479#31649#29702
          ImageIndex = 3
          OnExit = tsxitglExit
          object rzp10: TRzGroupBox
            Left = 0
            Top = 0
            Width = 243
            Height = 458
            Align = alLeft
            Alignment = taCenter
            BannerHeight = 24
            BorderOuter = fsFlat
            Caption = #31995#32479#31649#29702
            Font.Charset = GB2312_CHARSET
            Font.Color = clNavy
            Font.Height = -12
            Font.Name = #23435#20307
            Font.Style = [fsBold]
            GradientColorStyle = gcsCustom
            GroupStyle = gsBanner
            ParentColor = True
            ParentFont = False
            TabOrder = 0
            VisualStyle = vsClassic
            object rztreexitgl: TRzCheckTree
              Left = 1
              Top = 25
              Width = 241
              Height = 432
              Align = alClient
              Indent = 19
              SelectionPen.Color = clBtnShadow
              StateImages = rztreexitgl.CheckImages
              TabOrder = 0
            end
          end
        end
        object tsjicsj: TTabSheet
          Caption = #22522#30784#25968#25454
          ImageIndex = 7
          OnExit = tsjicsjExit
          object rzp16: TRzGroupBox
            Left = 0
            Top = 0
            Width = 242
            Height = 458
            Align = alLeft
            Alignment = taCenter
            BannerHeight = 24
            BorderOuter = fsFlat
            Caption = #22522#30784#25968#25454
            Font.Charset = GB2312_CHARSET
            Font.Color = clNavy
            Font.Height = -12
            Font.Name = #23435#20307
            Font.Style = [fsBold]
            GradientColorStyle = gcsCustom
            GroupStyle = gsBanner
            ParentColor = True
            ParentFont = False
            TabOrder = 0
            VisualStyle = vsClassic
            object rztreejicsj: TRzCheckTree
              Left = 1
              Top = 25
              Width = 240
              Height = 432
              Align = alClient
              Indent = 19
              SelectionPen.Color = clBtnShadow
              StateImages = rztreejicsj.CheckImages
              TabOrder = 0
            end
          end
        end
        object tsxuexzx: TTabSheet
          Caption = #35780#23457#31649#29702
          ImageIndex = 2
          object rzp1: TRzGroupBox
            Left = 0
            Top = 0
            Width = 241
            Height = 458
            Align = alLeft
            Alignment = taCenter
            BannerHeight = 24
            BorderOuter = fsFlat
            Caption = #35780#23457#31649#29702
            Font.Charset = GB2312_CHARSET
            Font.Color = clNavy
            Font.Height = -12
            Font.Name = #23435#20307
            Font.Style = [fsBold]
            GradientColorStyle = gcsCustom
            GroupStyle = gsBanner
            ParentColor = True
            ParentFont = False
            TabOrder = 0
            VisualStyle = vsClassic
            object rztreexuexzx: TRzCheckTree
              Left = 1
              Top = 25
              Width = 239
              Height = 432
              Align = alClient
              Indent = 19
              SelectionPen.Color = clBtnShadow
              StateImages = rztreexuexzx.CheckImages
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  inherited txttopspace: TStaticText
    Width = 1114
  end
  inherited qry: TUpAdoQuery
    Left = 848
  end
  inherited ds: TDataSource
    DataSet = qry
    Left = 880
  end
  inherited pmkuaijcd: TUpPopupMenu
    Left = 583
  end
  object qryjiaosdy: TUpAdoQuery
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=admin;Persist Security Info=True;Us' +
      'er ID=sa;Initial Catalog=gouz01;Data Source=127.0.0.1,7788'
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterScroll = qryjiaosdyAfterScroll
    Parameters = <>
    SQL.Strings = (
      'select * from sys_jiaosdy')
    Left = 424
    Top = 176
  end
  object dsjiaosdy: TDataSource
    DataSet = qryjiaosdy
    Left = 456
    Top = 176
  end
  object qryjiaosyg: TUpAdoQuery
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=admin;Persist Security Info=True;Us' +
      'er ID=sa;Initial Catalog=gouz01;Data Source=127.0.0.1,7788'
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'select * from v_yuang')
    Left = 552
    Top = 176
  end
  object dsjiaosyg: TDataSource
    DataSet = qryjiaosyg
    Left = 584
    Top = 176
  end
  object upmnSelyuang_jiaosdy: TUpPopupMenu
    Left = 585
    Top = 248
    object NJiaoscy_add: TMenuItem
      Caption = #28155#21152#21592#24037
      OnClick = NJiaoscy_addClick
    end
  end
end
