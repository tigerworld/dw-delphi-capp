inherited frmxitgl_xiugkoul: Tfrmxitgl_xiugkoul
  Left = 333
  Top = 241
  Caption = #29992#25143#20462#25913#21475#20196
  ClientHeight = 201
  ClientWidth = 347
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 14
  inherited bvl1: TBevel
    Top = 158
    Width = 347
  end
  inherited pfrmtop: TUPanel
    Top = 161
    Width = 347
    TabOrder = 1
    inherited btnyes: TUpSpeedButton
      Left = 117
    end
    inherited btnquit: TUpSpeedButton
      Left = 227
    end
  end
  object grpxiugkoul: TUpGroupBox [2]
    Left = 25
    Top = 20
    Width = 294
    Height = 120
    Caption = #21475#20196#20462#25913
    TabOrder = 0
    object lbl1: TUpLabel
      Left = 36
      Top = 27
      Width = 91
      Height = 14
      Caption = #35831#36755#20837#26087#21475#20196':'
    end
    object lbl2: TUpLabel
      Left = 36
      Top = 52
      Width = 91
      Height = 14
      Caption = #35831#36755#20837#26032#21475#20196':'
    end
    object lbl3: TUpLabel
      Left = 8
      Top = 76
      Width = 119
      Height = 14
      Caption = #35831#20877#27425#36755#20837#26032#21475#20196':'
    end
    object edtoldpass: TUpEdit
      Left = 130
      Top = 24
      Width = 140
      Height = 22
      ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
      MaxLength = 6
      PasswordChar = '*'
      TabOrder = 0
      AutoInit = False
      HistoryValueOnOff = True
    end
    object edtnewpass1: TUpEdit
      Left = 130
      Top = 51
      Width = 140
      Height = 22
      ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
      MaxLength = 6
      PasswordChar = '*'
      TabOrder = 1
      AutoInit = False
      HistoryValueOnOff = True
    end
    object edtnewpass2: TUpEdit
      Left = 130
      Top = 78
      Width = 140
      Height = 22
      ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
      MaxLength = 6
      PasswordChar = '*'
      TabOrder = 2
      AutoInit = False
      HistoryValueOnOff = True
    end
  end
end
