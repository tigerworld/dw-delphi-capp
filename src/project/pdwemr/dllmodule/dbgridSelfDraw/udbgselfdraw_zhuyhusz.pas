unit udbgselfdraw_zhuyhusz;

interface

uses
  SysUtils,
  DB,
  Dialogs,
  Graphics,
  ADODB,
  Grids,
  Messages,
  Windows,
  Wwdbigrd,
  Wwdbgrid,
  UpWWDbGrid, udbgselfdraw_dbgid_define;

//住院护士站医嘱审核 for 长期医嘱；临时医嘱借用长期医嘱过程；
procedure dbgCalcCellColor_zhuy_husz_yizshenh_changqyz(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//住院护士站-医嘱管理 for 长期医嘱； 临时医嘱借用长期医嘱过程；
procedure dbgCalcCellColor_zhuy_husz_yizguanli_changqyz(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
  
implementation

//住院护士站医嘱审核 for 长期医嘱；临时医嘱借用长期医嘱过程；
procedure dbgCalcCellColor_zhuy_husz_yizshenh_changqyz(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  //默认为可编辑颜色;
  ABrush.Color := StringToColor(dbg_edit_color);
  
  //护士已经审核;
  if Field.DataSet.FieldByName('husshbs').AsInteger = 1 then
  begin
    ABrush.Color := StringToColor(clYiz_husYijShenh);
  end;

  //医生已经停嘱；显示为停嘱颜色
  if Field.DataSet.FieldByName('yistzbs').AsInteger = 1 then
  begin
    ABrush.Color := StringToColor(dbg_readonly_color);
  end;
  
  //高亮处理
  if Highlight then
  begin
    ABrush.Color := StringToColor(dbg_highlight_color);
    AFont.Color := clBlue;
  end;

  //组号; 按照组号奇偶数字错开显示不同的颜色
  if SameText(Field.FieldName, 'zuh') then
  begin
    if (Field.AsInteger mod 2) = 0 then
    begin
      ABrush.Color := RGB($00,$ff,$ff);
    end
    else
    begin
      ABrush.Color := RGB($ff,$69,$b4);
    end;
  end;

  //皮试结果
  if SameText(Field.FieldName, 'pisjg') then
  begin
    case Field.AsInteger of
      0:
      begin

      end;
      1:
      begin

      end;
      2: //皮试未通过
      begin
        AFont.Color := clRed;
      end;
    end;
  end;
end;
       
//住院护士站-医嘱管理 for 长期医嘱； 临时医嘱借用长期医嘱过程；
procedure dbgCalcCellColor_zhuy_husz_yizguanli_changqyz(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin       
  //默认为可编辑颜色;
  ABrush.Color := StringToColor(dbg_edit_color);
  
  //已停嘱
  if Field.DataSet.FieldByName('zxdzhuangt').AsInteger = 2 then
  begin
    ABrush.Color := StringToColor(dbg_readonly_color);
  end;
  //已执行   
  if Field.DataSet.FieldByName('zxdzhuangt').AsInteger = 1 then
  begin
    ABrush.Color := StringToColor(clYiz_husYijZhix);
  end;
  //医嘱已取消  
  if Field.DataSet.FieldByName('zxdzhuangt').AsInteger = 3 then
  begin
    ABrush.Color := StringToColor(dbg_readonly_color);
  end;
  
  //高亮处理
  if Highlight then
  begin
    ABrush.Color := StringToColor(dbg_highlight_color);
    AFont.Color := clBlue;
  end;

  //组号; 按照组号奇偶数字错开显示不同的颜色
  if SameText(Field.FieldName, 'zuh') then
  begin
    if (Field.AsInteger mod 2) = 0 then
    begin
      ABrush.Color := RGB($00,$ff,$ff);
    end
    else
    begin
      ABrush.Color := RGB($ff,$69,$b4);
    end;
  end;

  //皮试结果
  if SameText(Field.FieldName, 'pisjg') then
  begin
    case Field.AsInteger of
      0:
      begin

      end;
      1:
      begin

      end;
      2: //皮试未通过
      begin
        AFont.Color := clRed;
      end;
    end;
  end;

  //是否计费 
  if SameText(Field.FieldName, 'zxdjifbs') then
  begin
    case Field.AsInteger of
      0: //未计费
      begin
      end;
      1: //已计费
      begin    
        ABrush.Color := StringToColor(clYiz_husYijJif);
      end;
    end;
  end;

  //是否发药
  if SameText(Field.FieldName, 'zxdfayfs') then
  begin
    //是否已经计费
    if Field.DataSet.FieldByName('zxdjifbs').AsInteger = 0 then
    begin //尚未计费
    end
    else
    begin //已经计费
      ABrush.Color := StrToInt(dbg_readonly_color);
    end;
  end;

  //发药药房
  if SameText(Field.FieldName, 'yizfyyfmc') then
  begin
    //是否已经计费
    if Field.DataSet.FieldByName('zxdjifbs').AsInteger = 0 then
    begin //尚未计费
    end
    else
    begin //已经计费
      ABrush.Color := StrToInt(dbg_readonly_color);
    end;
  end;
end;
end.
